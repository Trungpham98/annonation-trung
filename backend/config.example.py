# flake8: NOQA
import os


basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = eval(os.getenv('DEBUG', 'False))
    TESTING = eval(os.getenv('TESTING', 'False'))
    SECRET_KEY = os.getenv('SECRET', 'secret_key_for_flask_sesssion')
    JWT_SECRET = os.getenv('JWT_SECRET', 'xoxo')

    LOG_FILE = os.getenv('LOG_FILE', 'drive-log.log')

    # REDIS
    REDIS_PASSWD = os.getenv('REDIS_PASSWD', '')
    REDIS_HOST = os.getnev('REDIS_HOST', 'localhost')
    REDIS_PORT = int(os.getnev('REDIS_PORT', 6379))
    REDIS_DB = int(os.getnev('REDIS_DB', 12))
    REDIS_URL = 'redis://:{passwd}@{host}:{port}/{db}'.format(passwd=REDIS_PASSWD,
                                                              host=REDIS_HOST,
                                                              port=REDIS_PORT,
                                                              db=REDIS_DB)  # DON'T CHANGE ME

    MONGO_DATABASE_URI = 'mongodb://{username}:{password}@{host}/{db}'.format(username=os.getenv('MONGO_USERNAME', ''),
                                                                              password=os.getenv('MONGO_PASSWORD', ''),
                                                                              db=os.getenv('MONGO_DATABASE_NAME', ''),
                                                                              host=os.getenv('MONGO_HOST', ''))
    MONGO_DATABASE_NAME = os.getenv('MONGO_DATABASE_NAME', '')
    # Cookie
    SESSION_COOKIE_NAME = os.getenv('SESSION_COOKIE_NAME', 'session')
    SESSION_COOKIE_DOMAIN = os.getenv('SESSION_COOKIE_DOMAIN', '.bizflycloud.vn')
    SESSION_COOKIE_HTTPONLY = eval(os.getenv('SESSION_COOKIE_HTTPONLY', 'False'))

    # Config for keystone
    KEYSTONE = {
        "auth_url": os.getenv('KEYSTONE.AUTH_URL', ''),
        "role": os.getenv('KEYSTONE.ROLE', ''),
        "role_creator": os.getenv('KEYSTONE.ROLE_CREATOR', ''),
        "user_admin": os.getenv('KEYSTONE.USER_ADMIN', ''),
        "password_admin": os.getenv('KEYSTONE.PASSWORD_ADMIN', ''),
        "tenant_admin": os.getenv('KEYSTONE.TENANT_ADMIN', '')
    }
    SEND_MAIL_URI = os.getenv('SEND_MAIL_URI', 'http://localhost:3000/v1')
    CDN_UPLOAD_DATA = {
        'host': os.getenv('CDN_UPLOAD_DATA.HOST', ''),
        'domain': os.getenv('CDN_UPLOAD_DATA.DOMAIN', ''),
        'secret_key': os.getenv('CDN_UPLOAD_DATA.SECRET_KEY', ''),
    }
    MAX_CONTENT_LENGTH = int(os.getenv('MAX_CONTENT_LENGTH', 2 * 1024 * 1024))  # max size file upload
    BASE_ENDPOINT_DRIVE_SERVICE = os.getenv('BASE_ENDPOINT_DRIVE_SERVICE', 'https://drive-api.bizfly.vn/')
    DRIVE_SECRET_KEY = os.getenv('DRIVE_SECRET_KEY', 'XOXO')

    BILLING = {
        "url": os.getenv('BILLING.URL', 'https://staging-billing.vccloud.vn'),
    }
    SEND_MESSAGE = {
        'username': os.getenv('SEND_MESSAGE.AMQP_USERNAME', ''),
        'password': os.getenv('SEND_MESSAGE.AMQP_PASSWORD', ''),
        'amqp_host': os.getenv('SEND_MESSAGE.AMQP_HOST', ''),
        "amqp_virtual": os.getenv('SEND_MESSAGE.AMQP_VIRTUAL', 'billing_v3'),
        "exchange": os.getenv('SEND_MESSAGE.AMQP_EXCHANGE', 'billing_v3_xchange'),
        "queue_name": os.getenv('SEND_MESSAGE.AMQP_QUEUE_NAME', 'billing_event_queue'),
        "routing_key": os.getenv('SEND_MESSAGE.AMQP_ROUTING_KEY', 'notify_event'),
    }
    CERT_URI = os.getenv('CERT_URI', 'https://hn-1.vccloud.vn:9100/')
    PHOTO_URI = os.getenv('PHOTO_URI', 'https://vccloud-static.mediacdn.vn/')
    LOGO_DEFAULT = os.getenv('LOGO_DEFAULT', 'https://cloud.bizfly.vn/images/logo_bizfly_vccloud.png')
    FAVICON_DEFAULT = os.getenv('FAVICON_DEFAULT', 'https://cloud.bizfly.vn/images/ico/favicon.png')
    SENTRY_DSN = os.getenv('SENTRY_DSN', 'https://a811e9e359e845de845311e9600bd030:bceff25e0d4e43cfa794d9c9cfe88189@sentry.paas.vn/100')
    SENDMAIL_SERVICE_URI = os.getenv('SENDMAIL_SERVICE_URI', 'https://send-mail-api.vccloud.vn/v1/')


class ProductionConfig(Config):
    pass


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True


config = {
    'production': ProductionConfig,
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'default': DevelopmentConfig,
}
