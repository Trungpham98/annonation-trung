import requests
import time
from flask import current_app as app
import os
from werkzeug.utils import secure_filename
from datetime import datetime

UPLOAD_FOLDER = 'file/images'
ALLOWED_EXTENSIONS = set(['png', 'jpeg', 'jpg'])


def allowed_file_photo(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def create_folder_photo(module_name):
    directory_ = UPLOAD_FOLDER + '/' + datetime.utcnow().strftime('%Y%m%d_%H%M%S') + '/' + module_name
    if not os.path.exists(directory_):
        os.makedirs(directory_)

    return directory_


def save_photo(file_, module_name):
    # if user does not select file, browser also
    # submit a empty part without filename
    if not file_ or file_.filename == '':
        return False, 'No selected file', {}

    if file_ and allowed_file_photo(file_.filename):
        directory_ = create_folder_photo(module_name)
        filename = secure_filename(file_.filename)
        file_.save(os.path.join(directory_, filename))
        return True, '', directory_ + '/' + filename

    return False, 'File upload must be png/jpeg/jpg', {}


def remove_photo(file_):
    try:
        if file_ and type(file_) == str:
            file_path = file_.split('/')[:3]
            file_path = '/'.join(file_path)
            if os.path.exists(file_path):
                import shutil
                shutil.rmtree(file_path)
    except:
        app.logger.error('Cannot remove file: {}'.format(file_))