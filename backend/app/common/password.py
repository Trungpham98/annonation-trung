import bcrypt


def hash_pass(password):
    password=password.encode('utf-8')
    return bcrypt.hashpw(password, bcrypt.gensalt())

def check_pass(password, password_hash):
    password=password.encode('utf-8')
    return bcrypt.hashpw(password, password_hash) == password_hash


if __name__ == "__main__":
    passw = 'cho'
    pass_hash = hash_pass(passw)
    print(pass_hash)

    print(check_pass(passw, pass_hash))

    print(check_pass('asdasd', pass_hash))