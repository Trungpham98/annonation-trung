from flask import current_app as app
import jwt
from datetime import datetime, timedelta
import calendar


def expired_at():
    expired_after = app.config['JWT_EXPIRED_AT']
    now = (datetime.utcnow() + timedelta(hours=expired_after)).timetuple()
    return calendar.timegm(now)


def convert_unix_to_datetime(dt):
    return datetime.utcfromtimestamp(dt)


def encode(params):
    """
    Encode params to token
    params: Dict
    """
    params['expired_at'] = expired_at()
    JWT_SECRET = app.config['JWT_SECRET']
    return jwt.encode(params, JWT_SECRET, algorithm='HS256')


def decode(token):
    """
    Decode a token
    """

    JWT_SECRET = app.config['JWT_SECRET']
    try:
        data = jwt.decode(token, JWT_SECRET, algorithm='HS256')
        if convert_unix_to_datetime(data['expired_at']) < datetime.utcnow():
            return {}
        return data
    except:
        return {}
