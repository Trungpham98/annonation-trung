# -*- coding: utf-8 -*-
from flask import g, abort, make_response, jsonify, current_app as app
from werkzeug.datastructures import FileStorage
from datetime import datetime, timedelta
from bson.objectid import ObjectId
from app.common.password import hash_pass
import time


def before_insert_members(items):
    for item in items:
        member_schema = app.data.driver.db['members']
        member = member_schema.find_one({
            "account": item['account'],
            "project": item['project'],
        })
        if member:
            return abort(422, 'member existed in project.')
