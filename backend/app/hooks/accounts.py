# -*- coding: utf-8 -*-
from flask import g, abort, make_response, jsonify, current_app as app
from werkzeug.datastructures import FileStorage
from datetime import datetime, timedelta
from bson.objectid import ObjectId
from app.common.password import hash_pass
import time


def before_insert_accounts(items):
    print('ok')
    for item in items:
        item['password'] = hash_pass(item['password'])
        accounts_schema = app.data.driver.db['accounts']
        acc = accounts_schema.find_one({"email": item['email']})
        if acc:
            return abort(422, 'email exist')

def before_update_accounts(updates, original):
    if 'avatar_file' in updates:
        logo = item.get('avatar_file', None)
        ok, msg, dir_ = save_photo(logo, 'avatar')
        print(ok,msg,dir_)
        if not ok:
            return abort(422, msg)
        item['avatar'] = dir_
