from flask import g, abort, make_response, jsonify, current_app as app
from app.common.upload_file import save_photo, remove_photo


def before_insert_images(items):
    print(items)
    for item in items:
        logo = item.get('path_file', None)
        if not logo: 
            return abort(422, 'Photo was required')
        ok, msg, dir_ = save_photo(logo, 'photo')
        print(ok,msg,dir_)
        if not ok:
            return abort(422, msg)
        item['path'] = dir_

        item.pop('path_file', None)