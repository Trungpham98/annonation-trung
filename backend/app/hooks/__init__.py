from flask import abort, current_app as app, g
from app.hooks.images import before_insert_images
from app.hooks.accounts import before_insert_accounts, before_update_accounts
from app.hooks.members import before_insert_members


def init_hooks(app):
    app.on_insert_images += before_insert_images
    app.on_insert_accounts += before_insert_accounts
    app.on_update_accounts += before_update_accounts
    app.on_update_members += before_insert_members
    return app
