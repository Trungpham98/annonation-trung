from flask import send_from_directory, make_response, jsonify, request, current_app as app
from flask_restful import Resource
import os


class GetFile(Resource):
    def get(self, filename):

        return send_from_directory(
            directory=os.path.join(os.getcwd(), 'file', 'images'),
            filename=filename)
            
            
            