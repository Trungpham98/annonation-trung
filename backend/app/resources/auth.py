# -*- coding: utf-8 -*-
from flask import g, abort, make_response, current_app as app, request, jsonify, session
from flask_restful import Resource, reqparse
from datetime import datetime
import json


class UserInfo(Resource):
    def get(self):
        """
        API lay thong tin user tu cookie
        ---
        tags:
          - User Information from cookie
        responses:
          200:
            description: lay thong tin thanh cong
            examples:
                email: trung.ph1@gmail.com
                fullname: "Pham Hieu Trung"
                token: 'asdbxqwe'
                phone: 0966086471
                ...
          400:
            description: lay thong tin that bai
        """
        return make_response(jsonify({"message": "ok"}))