from flask import Flask, request, abort, g, make_response, jsonify
from flask_restful import reqparse
from logging import Formatter, getLogger, INFO
from logging.handlers import RotatingFileHandler
from itsdangerous import URLSafeTimedSerializer, BadSignature
from config import config
from .extensions import swagger, cors
from .api import (v1 as api_v1_blueprint)
from werkzeug.exceptions import Forbidden
from datetime import datetime, timedelta
from eve import Eve
from app.hooks import init_hooks
from app.utils.helpers import (
    make_err_response,
    make_success_response
)

DEFAULT_BLUEPRINTS = [api_v1_blueprint]
DEFAULT_URL_PREFIX = ''

parser = reqparse.RequestParser()


def create_app(config_name, blueprints=DEFAULT_BLUEPRINTS):
    app = Eve(settings=config[config_name].EVE_SETTINGS)

    app.config.from_object(config[config_name])
    app.config['PAGE_SIZE'] = 10
    app.config['COOKIE_SERIALIZER'] = \
        URLSafeTimedSerializer(app.secret_key,
                               signer_kwargs={'key_derivation': 'hmac'})

    configure_logging(app)
    configure_extensions(app)
    configure_blueprints(app, blueprints)
    init_hooks(app)
    return app


def create_redis_connection(host='localhost', port=6379, db=3, password=None):
    return redis.Redis(host=host, port=port, db=db, password=password)


def configure_logging(app):
    import logging
    log_file = app.config['LOG_FILE']
    handler = logging.FileHandler(log_file)
    handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s'))
    app.logger.setLevel(logging.INFO)
    app.logger.addHandler(handler)
    # config sentry
    app.logger.addHandler(handler)


def configure_extensions(app):
    cors.init_app(app)
    swagger.init_app(app)


def configure_blueprints(app, blueprints):
    for blueprint in blueprints:
        url_prefix = DEFAULT_URL_PREFIX

        if blueprint.url_prefix is not None:
            url_prefix += blueprint.url_prefix

        app.register_blueprint(blueprint, url_prefix=url_prefix)
