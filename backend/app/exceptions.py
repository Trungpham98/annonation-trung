class ValidationError(Exception):
    def __init__(self, message, value):
        super(ValidationError, self).__init__(message)
        self.value = value

    def __str__(self):
        return '{}: {}'.format(self.message, self.value)


class VerificationError(Exception):
    def __init__(self, message, value):
        super(VerificationError, self).__init__(message)
        self.value = value

    def __str__(self):
        return '{}: {}'.format(self.message, self.value)