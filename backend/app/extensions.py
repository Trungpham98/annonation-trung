# flake8: NOQA
from flask_cors import CORS
from flasgger import Swagger

cors = CORS(supports_credentials=True)
swagger = Swagger()
