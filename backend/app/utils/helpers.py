# -*- coding: utf-8 -*-
from flask import make_response, jsonify, abort, g, current_app as app
from bson.objectid import ObjectId


def make_err_response(msg, code):
    return make_response(jsonify({
        "_status": "ERR",
        "_error": {
            "message": msg,
            "code": code
        }
    }), code)


def make_abort_response(msg, code):
    return abort(code, {
        "_status": "ERR",
        "_error": {
            "message": msg,
            "code": code
        }
    })


def make_success_response(msg, code):
    from flask import make_response, jsonify
    return make_response(jsonify({
        "_status": "SUCCESS",
        "_success": {
            "message": msg,
            "code": code
        }
    }), code)
