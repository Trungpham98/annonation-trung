from pymongo import MongoClient
from flask import current_app as app


def connect():
    client = MongoClient(app.config['MONGO_DATABASE_URI'])
    db = client.get_database()
    client.close()
    return client, db


def init_drive():
    client, db = connect()
    return db.drive


def init_package():
    client, db = connect()
    return db.package


def init_history():
    client, db = connect()
    return db.history
