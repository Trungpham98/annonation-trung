# flake8: noqa
from flask import Blueprint
from flask_restful import Api
from app.resources.auth import UserInfo
from app.resources.file import GetFile

v1 = Blueprint('v1', __name__)
endpoints = Api(v1)


endpoints.add_resource(UserInfo, '/user/info')
endpoints.add_resource(GetFile, '/file/images/<path:filename>')