#! /usr/bin/env python
import os
from flask_script import Manager, Server
from werkzeug.contrib.fixers import ProxyFix
from app import create_app


app = create_app(os.getenv('APP_CONFIG', 'default'))
app.wsgi_app = ProxyFix(app.wsgi_app)
manager = Manager(app)
manager.add_command("runserver", Server(host="127.0.0.1", port=5005))


@manager.shell
def make_shell_context():
    return dict(app=app)


if __name__ == '__main__':
    manager.run()
