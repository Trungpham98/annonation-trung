# flake8: NOQA
import os
from settings import settings

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = True
    TESTING = True
    SECRET_KEY = '17a122c1-e957-4b68-b7e5-88ec6a71ce80'
    JWT_SECRET = '17a122c1-e957-4b68-b7e5-88ec6a71ce80'
    JWT_EXPIRED_AT = 7  # 7 hours
    LOG_FILE = 'annonation-log-sso.log'

    # Cookie
    SESSION_COOKIE_NAME = 'session'
    SESSION_COOKIE_DOMAIN = '.bizflycloud.vn'
    SESSION_COOKIE_HTTPONLY = False

    # Config for keystone
    MONGO_USERNAME = ''
    MONGO_PASSWORD = ''
    MONGO_DB = 'annonation'
    MONGO_HOST = 'localhost'
    EVE_SETTINGS = settings(MONGO_USERNAME, MONGO_PASSWORD, MONGO_DB, MONGO_HOST)

    MONGO_DATABASE_URI = 'mongodb://{username}:{password}@{host}/{db}'.format(username=MONGO_USERNAME,
                                                                              password=MONGO_PASSWORD,
                                                                              db=MONGO_DB,
                                                                              host=MONGO_HOST)
    MONGO_DATABASE_NAME = 'staging_activities_log'

    MAX_CONTENT_LENGTH = 3 * 1024 * 1024  # max size file upload


class ProductionConfig(Config):
    DEBUG = True


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True


class StagingConfig(Config):
    DEBUG = True


config = {
    'production': ProductionConfig,
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'default': DevelopmentConfig,
}
