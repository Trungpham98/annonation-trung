accounts_schema = dict(
    name=dict(type='string', required=True),
    email=dict(type='string', required=True),
    password=dict(type='string', required=True),
    birthday=dict(type='datetime'),
    gender=dict(type='string', default='other'),
    avatar=dict(type='string', default=''),
    phone=dict(type='string', default=''),
)
members_schema = dict(
    account=dict(
        type='objectid',
        required=True,
        data_relation={
            'resource': 'accounts',
            'field': '_id',
            'embeddable': True,
        }),
    project=dict(
        type='objectid',
        required=True,
        data_relation={
            'resource': 'projects',
            'field': '_id',
            'embeddable': True,
        }),
)
projects_schema = dict(
    name=dict(type='string', required=True),
    label_type=dict(type='string', required=True),
    description=dict(type='string', required=False, default='')
    owner=dict(
        type='objectid',
        required=True,
        data_relation={
            'resource': 'projects',
            'resource': 'accounts',
            'field': '_id',
            'embeddable': True,
        }),
)
images_schema = dict(
    path=dict(type='string', required=False),
    status=dict(type='string', required=True),
    edited_by=dict(type='string', required=False, default=''),
    description=dict(type='string', required=False, default=''),    
    project=dict(
        type='objectid',
        required=True,
        data_relation={
            'resource': 'projects',
            'field': '_id',
            'embeddable': True,
        }),
)

label_schema = dict(
    name=dict(type='string', required=True),
    label_type=dict(type='string', required=True),
    location=dict(type='list', default=[]),  
    updated_by=dict(type='string', required=False, default=''),
    description=dict(type='string', required=False, default=''),
    image=dict(
        type='objectid',
        required=True,
        data_relation={
            'resource': 'images',
            'field': '_id',
            'embeddable': True,
        }),
)


# mongo --host [host] -u [username] -p [password]--authenticationDatabase [dbname]
def settings(username, password, db, host):
    return dict(
        MONGO_HOST=host,
        MONGO_DBNAME=db,
        MONGO_USERNAME=username,
        MONGO_PASSWORD=password,
        RENDERERS=['eve.render.JSONRenderer'],
        DATE_FORMAT="%Y-%m-%dT%H:%M:%S",
        MONGO_QUERY_BLACKLIST=['$where'],
        DOMAIN={
            'accounts': {
                'additional_lookup': {
                    'url': 'regex("[\w]+")',
                    'field': 'name'
                },
                "resource_methods": ['GET', 'POST'],
                'item_methods': ['DELETE', 'GET', 'PATCH'],
                'allow_unknown': True,
                'cache_control': 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0',
                'cache_expires': 0,
                'schema': accounts_schema
            },
            'members': {
                'additional_lookup': {
                    'url': 'regex("[\w]+")',
                    'field': '_id'
                },
                "resource_methods": ['GET', 'POST'],
                'item_methods': ['DELETE', 'GET', 'PATCH'],
                'allow_unknown': False,
                'cache_control': 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0',
                'cache_expires': 0,
                'schema': members_schema
            },
            'projects': {
                'additional_lookup': {
                    'url': 'regex("[\w]+")',
                    'field': 'name'
                },
                "resource_methods": ['GET', 'POST'],
                'item_methods': ['DELETE', 'GET', 'PATCH'],
                'allow_unknown': False,
                'cache_control': 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0',
                'cache_expires': 0,
                'schema': projects_schema
            },
            'images': {
                'additional_lookup': {
                    'url': 'regex("[\w]+")',
                    'field': 'path'
                },
                "resource_methods": ['GET', 'POST'],
                'item_methods': ['DELETE', 'GET', 'PATCH'],
                'allow_unknown': True,
                'cache_control': 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0',
                'cache_expires': 0,
                'schema': images_schema
            },
            'label': {
                'additional_lookup': {
                    'url': 'regex("[\w]+")',
                    'field': 'name'
                },
                "resource_methods": ['GET', 'POST'],
                'item_methods': ['DELETE', 'GET', 'PATCH'],
                'allow_unknown': False,
                'cache_control': 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0',
                'cache_expires': 0,
                'schema': label_schema
            },
        }
    )
